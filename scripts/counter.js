const ID_COUNTER_SECTION = 'count-section';
const ID_COUNT_VALUE = 'count-value';
const ID_COUNT_INCREASE = 'count-increase';
const ID_COUNT_DECREASE = 'count-decrease';
const ID_INC_DEC = 'inc-dec';

function init() {
  const initCount = 0;
  const elCountSection = document.getElementById(ID_COUNTER_SECTION);
  elCountSection.innerHTML = `
        <h2>Counter</h2>
        <p>Wert um 1 vermindern oder erhöhen</p>
        <div id="${ID_INC_DEC}">
            <button id="${ID_COUNT_DECREASE}">-</button>
            <p id="${ID_COUNT_VALUE}">${initCount}</p>
            <button id="${ID_COUNT_INCREASE}">+</button>
        </div>
    `;
  const counter = createCounter(initCount);
  const elDecBtn = document.getElementById(ID_COUNT_DECREASE);
  elDecBtn.addEventListener('click', getChangeCountValueCb(counter, -1));
  const elIncBtn = document.getElementById(ID_COUNT_INCREASE);
  elIncBtn.addEventListener('click', getChangeCountValueCb(counter, 1));
}

function getChangeCountValueCb(counter, change) {
  return () => {
    const elCountValue = document.getElementById(ID_COUNT_VALUE);
    elCountValue.textContent = counter.next(change);
  };
}

function createCounter(count = 0) {
  return {
    next: (change) => {
      return (count += change);
    },
  };
}

export { init };
