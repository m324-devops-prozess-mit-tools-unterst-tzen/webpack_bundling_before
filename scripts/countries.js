const ID_COUNTRY_SECTION = 'country-section';
const ID_COUNTRY_LIST = 'country-list';

function init() {
  const elCountrySection = document.getElementById(ID_COUNTRY_SECTION);
  elCountrySection.innerHTML = `
        <h2>
            Länderkürzel von 
            <a href="https://restcountries.com/#endpoints-all">REST Countries API</a>
        </h2>
        <ul id="${ID_COUNTRY_LIST}">
        </ul>
    `;
  const elCountryList = document.getElementById(ID_COUNTRY_LIST);
  addCountriesCodes(elCountryList);
}

async function addCountriesCodes(elCountryList) {
  const countries = await axios
    .get('https://restcountries.com/v3.1/all')
    .then((response) => response.data);
  for (let i = 0; i < countries.length; i++) {
    const country = countries[i];
    const code = country?.cca2;
    if (code) {
      const elCode = document.createElement('li');
      elCode.textContent = code;
      elCountryList.appendChild(elCode);
    }
  }
}

export { init };
