import { init as initColorSelection } from './colorSelection.js';
import { init as initCounter } from './counter.js';
import { init as initCountries } from './countries.js';

initCounter();
initColorSelection();
initCountries();
