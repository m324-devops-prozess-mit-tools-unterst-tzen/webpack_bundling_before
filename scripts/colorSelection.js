const ID_COLOR_SECTION = 'color-section';
const ID_COLOR_CONTENT = 'color-content';
const ID_COLOR_DISPLAY = 'color-display';
const ID_COLOR_SELECTION = 'color-selection';

function init() {
  const elColorSection = document.getElementById(ID_COLOR_SECTION);
  elColorSection.innerHTML = `
        <h2>Farbe wählen</h2>
        <div id="${ID_COLOR_CONTENT}">
            <div id="${ID_COLOR_DISPLAY}" class="black"></div>
            <div id="${ID_COLOR_SELECTION}">
                <input
                id="black"
                type="radio"
                name="color"
                value="black"
                checked
                />
                <label for="black">Schwarz</label>
                <br />
                <input id="red" type="radio" name="color" value="red" />
                <label for="red">Rot</label>
                <br />
                <input id="green" type="radio" name="color" value="green" />
                <label for="green">Grün</label>
                <br />
                <input id="blue" type="radio" name="color" value="blue" />
                <label for="blue">Blau</label>
                <br />
                <input id="yellow" type="radio" name="color" value="yellow" />
                <label for="yellow">Gelb</label>
            </div>
        </div>
    `;
  const elColorDisplay = document.getElementById(ID_COLOR_DISPLAY);
  const elColorRadios = document.querySelectorAll(
    `#${ID_COLOR_SELECTION} input[name="color"]`
  );
  for (let i = 0; i < elColorRadios.length; i++) {
    const elColorRadio = elColorRadios[i];
    elColorRadio.addEventListener('change', () => {
      elColorDisplay.className = elColorRadio.value;
    });
  }
}

export { init };
